import numpy

li = [line.rstrip() for line in open('input')]
count = 0
# first part
while count < 12:
    sample = [x[count] for x in li]
    fdist = dict(zip(*numpy.unique(sample, return_counts=True)))
    #print(fdist)
    if '0' not in fdist:
        li = [x for x in li if x[count] == '1']
    elif '1' not in fdist:
        li = [x for x in li if x[count] == '0']
    elif fdist['0'] > fdist['1']:
        li = [x for x in li if x[count] == '0']
    elif fdist['0'] < fdist['1']:
        li = [x for x in li if x[count] == '1']
    else:
        li = [x for x in li if x[count] == '1']
    #
    if (len(li) == 1):
        res1 = bin(int("0b" + li[0], 2))
        print("Final binary number: " + res1)
        break
    #
    count += 1
   
li = [line.rstrip() for line in open('input')]
count = 0
#second part
while count < 12:
    sample = [x[count] for x in li]
    fdist = dict(zip(*numpy.unique(sample, return_counts=True)))
    #print(fdist)
    if '0' not in fdist:
        li = [x for x in li if x[count] == '0']
    elif '1' not in fdist:
        li = [x for x in li if x[count] == '1']
    elif fdist['0'] > fdist['1']:
        li = [x for x in li if x[count] == '1']
    elif fdist['0'] < fdist['1']:
        li = [x for x in li if x[count] == '0']
    else:
        li = [x for x in li if x[count] == '0']
    #
    if (len(li) == 1):
        res2 = bin(int("0b" + li[0], 2))
        print("Final binary number: " + res2)
        break
    #
    count += 1

print("Total: " + str(int(res1, 2)*int(res2, 2))) 
